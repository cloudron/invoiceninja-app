This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin@cloudron.local<br/>
**Password**: changeme<br/>

Please change the admin password and email immediately.
